#lol this is farecalc in python

from sys import argv as args
from sys import stdin as infile
from math import fabs

def manhattan_distance(a,b,c,d,u1,v1,u2,v2):
    x1 = a*u1 + b*v1
    y1 = c*u1 + d*v1
    x2 = a*u2 + b*v2
    y2 = c*u2 + d*v2
    return int(fabs(x1-x2) + fabs(y1-y2))

args = [int(arg) for arg in args[1:]]
a,b,c,d = args[0], args[1], args[2], args[3]

for line in infile:
    vs = [int(v) for v in line.split()]
    u1,v1,u2,v2 = vs[0], vs[1], vs[2], vs[3]
    print manhattan_distance(a,b,c,d,u1,v1,u2,v2)
