// fare calculator

#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char* argv[])
{
    if (argc!=5)
    {
      fprintf(stderr, "Farecalc require four arguments (a b c d)");
      return 1;
    }

    else
    {
      char* strend; //pointer for the strtol arguments. not actually used
      long int a = strtol(argv[1], &strend, 10); //coordinate modifiers passed as command-line arguments.
      long int b = strtol(argv[2], &strend, 10);
      long int c = strtol(argv[3], &strend, 10);
      long int d = strtol(argv[4], &strend, 10);

			int u1, u2, v1, v2;

      while ((scanf("%d%d%d%d", &u1, &v1, &u2, &v2)) !=EOF)
      {

           long int x1, x2, y1, y2;

           x1 = a*u1 + b*v1;
           x2 = a*u2 + b*v2;
           y1 = c*u1 + d*v1;
           y2 = c*u2 + d*v2;

           long int fare = abs(x1-x2) + abs(y1-y2);
           printf("%ld\n", fare);

					u1 = 0;
					u2 = 0;
					v1 = 0;
					v2 = 0;
        }
    }

    return 0;
}
