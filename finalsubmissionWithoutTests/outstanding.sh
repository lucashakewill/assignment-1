#! /usr/bin/env bash

# make temporary directory 
mkdir -p ./tmp/
rm -f ./tmp/fares.txt 
rm -f ./tmp/namebalance.txt

# check for valid values
cat $1 | ./checksum | 

#cut out u1, v1, u2 and v2 from the line, with space between each, send it to farecalc
#and that out to another temp file 

cut -d \: -f 4-5 |

# replaces the : and , with whitespace, send to farecalc and save as fares

sed -r  's/[:,]/\ /g' | ./farecalc $2 $3 $4 $5 | cat >./tmp/fares.txt

# make a file that displays name:balance
cut -d \: -f 2-3 $1 | cat >./tmp/namebalance.txt 

#make a file that displays name:balance:fare charged
paste -d \: ./tmp/namebalance.txt ./tmp/fares.txt |

gawk -F: '{ if (($2-$3) < 0) print $1 }'

rm -rf ./tmp

  


