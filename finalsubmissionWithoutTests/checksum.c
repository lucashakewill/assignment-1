/* checksum reads a line at a time from stdin until EOF. It writes the line
   to stdout if the first ten characters are composed of the digits 0-9,
   and the tenth digit is equal to the last digit of the sum of the first 9
   digits*/

#include <stdio.h>
#include <string.h>


int main(void) {


    char storage[100];

    while (scanf("%99s", storage) != EOF)
    {
      if (strlen(storage)<10) /*is the line at least 10 characters long, otherwise go to next line*/
        continue;

      int sum=0;

      for(int i=0; i<9; i++) /*store sum of the first 9 digits. go to next line if invalid character*/
      {
        if (storage[i] < '0' || storage[i] > '9')
            break;

        else
            sum += storage[i]-'0';

      }


        if ((sum %10) == storage[9]-'0')
          {
            printf("%s\n", storage);
          }
    }



    return 0;
}
