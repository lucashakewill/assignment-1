/#! /usr/bin/env bash

cat $1 | ./checksum | cut -d \: -f 3 | paste -s -d+ | bc
